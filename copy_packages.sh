# copy hacked files from share diretory (FIXME)
# scp share/TtresEventSaverFlatNtuple.h   source/HQTTtResonancesTools/HQTTtResonancesTools/
# scp share/NeutrinoWeighter.h            source/HQTTtResonancesTools/HQTTtResonancesTools/
# scp share/TtresEventSaverFlatNtuple.cxx source/HQTTtResonancesTools/Root/
# scp share/NeutrinoWeighter.cxx          source/HQTTtResonancesTools/Root/

# copy config files for offline code to run diretory
scp source/TQPTtbarResDilep/share/offline_configuration.txt   run/
scp source/TQPTtbarResDilep/share/VariablesToSave.txt         run/
scp source/TQPTtbarResDilep/share/VariablesToRead.txt         run/
scp source/TQPTtbarResDilep/share/weights.txt                 run/
# scp source/TQPTtbarResDilep/share/online_dilepton*.txt        run/
# scp source/TQPTtbarResDilep/share/online_input_test*.txt      run/

# copy grid diretory
# mkdir grid
# scp source/TQPTtbarResDilep/share/grid/*                      grid/
