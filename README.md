NB: this repository is obsolete. Please use: https://gitlab.cern.ch/atlas-phys/exot/hqt/tt_tb_Resonances_Run2/ttbar_packages/ttresdilep



# Setup and preliminary steps

## To setup (AT version 21.2.115)

   * For the first time:
   ```
   git clone ssh://git@gitlab.cern.ch:7999/pinamont/ttresdilep.git ttResDilep-21.2.115
   cd ttResDilep-21.2.115
   source build.sh 21.2.115
   ```
   (if you want to setup a different version, just specify a different tag afeter the `source build.sh` command; if you don't sepcify anything version 21.2.89 is set-up)
   
   * To setup when you came back:
   ```
   source setup.sh
   ```
   
   * To recompile:
   ```
   source recompile.sh
   ```
   
   * To clean:
   ```
   rm -rf build
   ```

   * To update:
   ```
   git pull
   git submodule init
   git submodule update
   ```
   
## To test

   * L1 code (on TS farm):
   ```
   cd run
   top-xaod ttres-dilep-cuts-mc16d-noSyst.txt input_test_farmts.txt
   cd ..
   ```
   
   * L2 code (on the previously created output file):
   ```
   cd runL2
   echo ../run/output.root > offline_inputs.txt
   top-offline offline_configuration.txt offline_inputs.txt
   cd ..
   ```

---
   
# To run on the grid (L1 code only)
   
## To setup grid

   * Run the grid seutp script and move to the `grid` directory (provided that a valid grid certificate is present):
   ```
   source grid_setup.sh
   cd grid
   ```
   
## To lanch a production on the grid

   * Move inside the `grid` directory (and make sure you run the `setup_grid.sh` script before)
   * Eventually modify the .py files `MC*_v*.py`, `Data_v*.py` and `submit_*_v21.py` (and `check_samples_*.py`)
   * Run first the proper check script `check_samples_*.py`, which cheks that all the samples are there on the grid, eg:
   ```
   python check_samples_v21a.py
   ```
   * If everything is fine, run the submission script, eg:
   ```
   python submit_all_v21a.py
   ```
   * As soon as jobs are submitted, monitor them from the web browser, e.g.: https://bigpanda.cern.ch/tasks/?taskname=user.pinamont*V21a*&days=30
   * When all jobs are at least "registered", you can already request the replication to local RSE (usually better than specify a destination from the submission script):
      * go to https://rucio-ui.cern.ch/
      * then "Data Transfers", "Request new rule"
   
## To run on the produced ntuples the offline code

   * Step 1: create the list of samples per group (one sample per DSID, while groups have the same names of the actual L2 ntuples that you will create):
      * go to `grid` directory (making sure you setup the grid environment before)
      * run the script(s) `dump_outputs_*.py` that creates a list of samples per group in a new directoy with the name of the ntuple priduction tag (e.g. `V21a/`)
      * eg:
      ```
      source grid_setup.sh
      cd grid
      python dump_outputs_V21a.py
      python dump_outputs_V21d.py
      python dump_outputs_V21e.py
      cd ..
      ```
   * Step 2: create links to the outptu L1 ntuples:
      * go to the `runL2` directory and modify the `link_all_samples.sh` according to your needs
      * run the script: `link_all_samples.sh` that will create in `gpfs` (Trieste farm) the links to the `.root` files in local-group-disk, as well as lists of all input files per sample, automatically created in the `inputs/` directory
      * eg:
      ```
      cd runL2
      source link_all_samples.sh
      ```
   * Step 3: run the actual offline code:
      * go to / stay in the `runL2` directory
      * modify the `run_L2.sh` script, specifying data and MC samples separately, as well as output directory and eventually queue to be used (if want to submit batch jobs - only for pbs / bsub)
      * run the script:
      ```
      source run_L2.sh
      ```

   
   
<!-----

# Older instructions:

## To setup (AT version 21.2.89)

   * For the first time:
   ```
   git clone ssh://git@gitlab.cern.ch:7999/pinamont/ttresdilep.git ttResDilep-21.2.89
   cd ttResDilep-21.2.89
   source build.sh
   ```
   
   * To setup when you came back:
   ```
   source setup.sh
   ```
   
   * To recompile:
   ```
   source recompile.sh
   ```
   
   * To clean:
   ```
   rm -rf build
   ```

   * To update:
   ```
   git pull
   git submodule init
   git submodule update
   ```-->
   
   
<!--## To setup with centOS7 (experimental)

   * Before running the `build.sh` script, do the following:
   ```
   export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase; source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -c centos7
   ```-->
   
   
<!--## To setup AT version 21.2.97 (or higher)

   ```
   git clone ssh://git@gitlab.cern.ch:7999/pinamont/ttresdilep.git ttResDilep-21.2.97
   cd ttResDilep-21.2.97
   source build.sh 21.2.97
   ```

   * and then proceed as above-->
