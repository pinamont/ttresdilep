#!/usr/bin/env python
import getpass
import os
import TopExamples.grid
import Data_v21
#import MC16a_v21
import MC16d_v21
#import MC16e_v21

# Configuration
config = TopExamples.grid.Config()
config.CMake         = True
config.code          = 'top-xaod'
#config.settingsFile  = 'ttres-dilep-cuts-mc16a-noSyst.txt'
#config.settingsFile  = 'ttres-dilep-cuts-mc16d-allSyst.txt'
#config.settingsFile  = 'ttres-dilep-cuts-mc16e-allSyst.txt'
#config.settingsFile  = 'ttres-dilep-cuts-mc16a-noSyst.txt'
config.settingsFile  = 'ttres-dilep-cuts-mc16d-noSyst.txt'
#config.settingsFile  = 'ttres-dilep-cuts-mc16e-noSyst.txt'
#config.gridUsername  = 'madesant'
#config.gridUsername  = 'mfaraj'
config.gridUsername  = 'pinamont'
#config.suffix        = 'V21d'
config.suffix        = 'V21d-bis'
#config.suffix        = 'V21d-syst'
#config.excludedSites = ''
config.excludedSites = 'ANALY_SCINET'
#config.excludedSites = 'INFN-NAPOLI'
config.noSubmit      = False
config.mergeType     = 'None' #'None', 'Default' or 'xAOD'
#config.mergeType     = 'Default'
#config.memory        = '1000'
config.destSE        = 'INFN-TRIESTE_LOCALGROUPDISK'
config.maxNFilesPerJob  = '5'
#config.maxNFilesPerJob  = '1'

# -- additional options
# to use when resubmitting 'broken' tasks
#config.otherOptions  = '--allowTaskDuplication'
 
names = [
         ## Data
         #'data15',
         #'data16',
         #'data17',
         #'data18',
         ## Nominal ttbar
         #'ttbar_nah_mcd',                                                                                                                                             
         #'ttbar_dil_mcd',
         ## Nominal single top
         #'sintop_mcd',
         #'sintop_wt_mcd',
         #'sintop_wt_dilep_mcd',
         ## Nominal other bkg
         #'diboson_mcd',
         #'zjets_mcd',
         #'wjets_mcd',
         #'ttV_mcd',
         ## Alternative ttbar
         #'ttbar_dil_aMCP8_mcd',
         #'ttbar_dil_PH7old_mcd',
         #'ttbar_dil_aMCH7_mcd',
         #'ttbar_dil_PH7_mcd',
         #'ttbar_dil_hdamp_mcd',
         ## Alternative single top
         #'sintop_wt_dilep_DS_mcd',
         #'sintop_wt_dilep_PH7_mcd',
         #'sintop_wt_dilep_aMCP8_mcd',
         ## Singal
         #'zprime400_mcd',
         #'zprime500_mcd',
         #'zprime750_mcd',
         #'zprime1000_mcd',
         #'zprime1250_mcd',
         #'zprime1500_mcd',
         #'zprime1750_mcd',
         #'zprime2000_mcd',
         #'zprime2250_mcd',
         #'zprime2500_mcd',
         #'zprime2750_mcd',
         'zprime3000_mcd',
         #'zprime4000_mcd',
         #'zprime5000_mcd',
         ##'mc16a_KKgluon_tt',   # missing proper derivation tag ?
         ##'mc16a_graviton_tt',  # crashing...
         #'grav400_mcd',
         #'grav500_mcd',
         #'grav750_mcd',
         #'grav1000_mcd',
         #'grav2000_mcd',
         #'grav3000_mcd',
         ##'mc16a_HVT_tt',       # missing x-sections
         ]
samples = TopExamples.grid.Samples(names)

# copy the configuration file from the run directory
os.system("scp ../run/"+config.settingsFile+" .")

# Submit Jobs
#TopExamples.ami.check_sample_status(samples)
TopExamples.grid.submit(config, samples)
