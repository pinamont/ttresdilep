#!/usr/bin/env python
import getpass
import os
import TopExamples.grid
import Data_v21
#import MC16a_v21
#import MC16d_v21
import MC16e_v21

names = [
         # Data
         'data18',
         ]
names+= [
         # Nominal ttbar
         'ttbar_nah_mce',                                                                                                                                             
         'ttbar_dil_mce',
         # Nominal single top
         'sintop_mce',
         'sintop_wt_mce',
         'sintop_wt_dilep_mce',
         # Nominal other bkg
         'diboson_mce',
         'zjets_mce',
         'wjets_mce',
         'ttV_mce',
         # Alternative ttbar
         'ttbar_dil_aMCP8_mce',
         'ttbar_dil_PH7old_mce',
         'ttbar_dil_aMCH7_mce',
         'ttbar_dil_PH7_mce',
         'ttbar_dil_hdamp_mce',
         # Alternative single top
         'sintop_wt_dilep_DS_mce',
         'sintop_wt_dilep_PH7_mce',
         'sintop_wt_dilep_aMCP8_mce',
         # Singal
         'zprime400_mce',
         'zprime500_mce',
         'zprime750_mce',
         'zprime1000_mce',
         'zprime1250_mce',
         'zprime1500_mce',
         'zprime1750_mce',
         'zprime2000_mce',
         'zprime2250_mce',
         'zprime2500_mce',
         'zprime2750_mce',
         'zprime3000_mce',
         'zprime4000_mce',
         'zprime5000_mce',
         ]
samples = TopExamples.grid.Samples(names)

print "**********************************************************"
for sample in samples:
    print "------------------------------------"
    print "Checking "+sample.name
    print "------------------------------------"
    for ds in sample.datasets:
        print ds
        os.system("rucio ls "+ds+" | grep CONTAINER")
        
print "------------------------------------"
