#!/usr/bin/env python
import getpass
import os
import TopExamples.grid
import Data_v21
import MC16a_v21
#import MC16d_v21
#import MC16e_v21

# Configuration
config = TopExamples.grid.Config()
config.CMake         = True
config.code          = 'top-xaod'
#config.settingsFile  = 'ttres-dilep-cuts-mc16a-allSyst.txt'
#config.settingsFile  = 'ttres-dilep-cuts-mc16d-allSyst.txt'
#config.settingsFile  = 'ttres-dilep-cuts-mc16e-allSyst.txt'
config.settingsFile  = 'ttres-dilep-cuts-mc16a-noSyst.txt'
#config.settingsFile  = 'ttres-dilep-cuts-mc16d-noSyst.txt'
#config.settingsFile  = 'ttres-dilep-cuts-mc16e-noSyst.txt'
#config.gridUsername  = 'madesant'
#config.gridUsername  = 'mfaraj'
config.gridUsername  = 'pinamont'
#config.suffix        = 'V21a'
config.suffix        = 'V21a-bis'
#config.suffix        = 'V21a-syst'
#config.excludedSites = ''
config.excludedSites = 'ANALY_SCINET'
#config.excludedSites = 'INFN-NAPOLI'
config.noSubmit      = False
#config.mergeType     = 'None' #'None', 'Default' or 'xAOD'
config.mergeType     = 'Default'
#config.memory        = '1000'
config.destSE        = 'INFN-TRIESTE_LOCALGROUPDISK'
config.maxNFilesPerJob  = '5'
#config.maxNFilesPerJob  = '1'

# -- additional options
# to use when resubmitting 'broken' tasks
#config.otherOptions  = '--allowTaskDuplication'
 
names = [
         ## Data
         #'data15',
         'data16',
         ####'data17',
         ####'data18',
         ### Nominal ttbar
         #'ttbar_nah_mca',                                                                                                                                             
         #'ttbar_dil_mca',
         ## Nominal single top
         #'sintop_mca',
         #'sintop_wt_mca',
         #'sintop_wt_dilep_mca',
         ## Nominal other bkg
         #'diboson_mca',
         #'zjets_mca',
         #'wjets_mca',
         #'ttV_mca',
         ## Alternative ttbar
         #'ttbar_dil_aMCP8_mca',
         #'ttbar_dil_PH7old_mca',
         #'ttbar_dil_aMCH7_mca',
         #'ttbar_dil_PH7_mca',
         #'ttbar_dil_hdamp_mca',
         ## Alternative single top
         #'sintop_wt_dilep_DS_mca',
         #'sintop_wt_dilep_PH7_mca',
         #'sintop_wt_dilep_aMCP8_mca',
         ## Singal
         #'zprime400_mca',
         #'zprime500_mca',
         #'zprime750_mca',
         #'zprime1000_mca',
         #'zprime1250_mca',
         #'zprime1500_mca',
         #'zprime1750_mca',
         #'zprime2000_mca',
         #'zprime2250_mca',
         #'zprime2500_mca',
         #'zprime2750_mca',
         #'zprime3000_mca',
         #'zprime4000_mca',
         #'zprime5000_mca',
         ##'mc16a_KKgluon_tt',   # missing proper derivation tag ?
         ##'mc16a_graviton_tt',  # crashing...
         ##'mc16a_HVT_tt',       # missing x-sections
         ]
samples = TopExamples.grid.Samples(names)

# copy the configuration file from the run directory
os.system("scp ../run/"+config.settingsFile+" .")

# Submit Jobs
#TopExamples.ami.check_sample_status(samples)
TopExamples.grid.submit(config, samples)
