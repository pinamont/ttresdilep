# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

import TopExamples.grid

TopExamples.grid.Add("ttbar_nah_mcd").datasets = [
    'mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_TOPQ1.e6337_s3126_r10201_p4031',
    ]

TopExamples.grid.Add("ttbar_dil_mcd").datasets = [
    'mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_TOPQ1.e6348_s3126_r10201_p4031',
    ]

TopExamples.grid.Add("sintop_mcd").datasets = [
    'mc16_13TeV.410658.PhPy8EG_A14_tchan_BW50_lept_top.deriv.DAOD_TOPQ1.e6671_s3126_r10201_p4031',
    'mc16_13TeV.410659.PhPy8EG_A14_tchan_BW50_lept_antitop.deriv.DAOD_TOPQ1.e6671_s3126_r10201_p4031',
    'mc16_13TeV.410644.PowhegPythia8EvtGen_A14_singletop_schan_lept_top.deriv.DAOD_TOPQ1.e6527_s3126_r10201_p4031',
    'mc16_13TeV.410645.PowhegPythia8EvtGen_A14_singletop_schan_lept_antitop.deriv.DAOD_TOPQ1.e6527_s3126_r10201_p4031',
    ]

TopExamples.grid.Add("sintop_wt_mcd").datasets = [
    'mc16_13TeV.410646.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_top.deriv.DAOD_TOPQ1.e6552_s3126_r10201_p4031',
    'mc16_13TeV.410647.PowhegPythia8EvtGen_A14_Wt_DR_inclusive_antitop.deriv.DAOD_TOPQ1.e6552_s3126_r10201_p4031',
    ]

TopExamples.grid.Add("sintop_wt_dilep_mcd").datasets = [
    'mc16_13TeV.410648.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_top.deriv.DAOD_TOPQ1.e6615_s3126_r10201_p4031',
    'mc16_13TeV.410649.PowhegPythia8EvtGen_A14_Wt_DR_dilepton_antitop.deriv.DAOD_TOPQ1.e6615_s3126_r10201_p4031',
    ]

TopExamples.grid.Add("diboson_mcd").datasets = [
    'mc16_13TeV.363356.Sherpa_221_NNPDF30NNLO_ZqqZll.deriv.DAOD_TOPQ1.e5525_s3126_r10201_p4029',
    'mc16_13TeV.363358.Sherpa_221_NNPDF30NNLO_WqqZll.deriv.DAOD_TOPQ1.e5525_s3126_r10201_p4029',
    'mc16_13TeV.364250.Sherpa_222_NNPDF30NNLO_llll.deriv.DAOD_TOPQ1.e5894_s3126_r10201_p4029',
    'mc16_13TeV.364253.Sherpa_222_NNPDF30NNLO_lllv.deriv.DAOD_TOPQ1.e5916_s3126_r10201_p4029',
    'mc16_13TeV.364254.Sherpa_222_NNPDF30NNLO_llvv.deriv.DAOD_TOPQ1.e5916_s3126_r10201_p4029',
    ]

TopExamples.grid.Add("zjets_mcd").datasets = [                                
    'mc16_13TeV.364100.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_TOPQ1.e5271_s3126_r10201_p4029',
    'mc16_13TeV.364101.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_TOPQ1.e5271_s3126_r10201_p4029',
    'mc16_13TeV.364102.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV0_70_BFilter.deriv.DAOD_TOPQ1.e5271_s3126_r10201_p4029',
    'mc16_13TeV.364103.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_TOPQ1.e5271_s3126_r10201_p4029',
    'mc16_13TeV.364104.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_TOPQ1.e5271_s3126_r10201_p4029',
    'mc16_13TeV.364105.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV70_140_BFilter.deriv.DAOD_TOPQ1.e5271_s3126_r10201_p4029',
    'mc16_13TeV.364106.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_TOPQ1.e5271_s3126_r10201_p4029',
    'mc16_13TeV.364107.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_TOPQ1.e5271_s3126_r10201_p4029',
    'mc16_13TeV.364108.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV140_280_BFilter.deriv.DAOD_TOPQ1.e5271_s3126_r10201_p4029',
    'mc16_13TeV.364109.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_TOPQ1.e5271_s3126_r10201_p4029',
    'mc16_13TeV.364110.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_TOPQ1.e5271_s3126_r10201_p4029',
    'mc16_13TeV.364111.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV280_500_BFilter.deriv.DAOD_TOPQ1.e5271_s3126_r10201_p4029',
    'mc16_13TeV.364112.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV500_1000.deriv.DAOD_TOPQ1.e5271_s3126_r10201_p4029',
    'mc16_13TeV.364113.Sherpa_221_NNPDF30NNLO_Zmumu_MAXHTPTV1000_E_CMS.deriv.DAOD_TOPQ1.e5271_s3126_r10201_p4029',
    'mc16_13TeV.364114.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_TOPQ1.e5299_s3126_r10201_p4029',
    'mc16_13TeV.364115.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_TOPQ1.e5299_s3126_r10201_p4029',
    'mc16_13TeV.364116.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV0_70_BFilter.deriv.DAOD_TOPQ1.e5299_s3126_r10201_p4029',
    'mc16_13TeV.364117.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_TOPQ1.e5299_s3126_r10201_p4029',
    'mc16_13TeV.364118.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_TOPQ1.e5299_s3126_r10201_p4029',
    'mc16_13TeV.364119.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV70_140_BFilter.deriv.DAOD_TOPQ1.e5299_s3126_r10201_p4029',
    'mc16_13TeV.364120.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_TOPQ1.e5299_s3126_r10201_p4029',
    'mc16_13TeV.364121.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_TOPQ1.e5299_s3126_r10201_p4029',
    'mc16_13TeV.364122.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV140_280_BFilter.deriv.DAOD_TOPQ1.e5299_s3126_r10201_p4029',
    'mc16_13TeV.364123.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_TOPQ1.e5299_s3126_r10201_p4029',
    'mc16_13TeV.364124.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_TOPQ1.e5299_s3126_r10201_p4029',
    'mc16_13TeV.364125.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV280_500_BFilter.deriv.DAOD_TOPQ1.e5299_s3126_r10201_p4029',
    'mc16_13TeV.364126.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV500_1000.deriv.DAOD_TOPQ1.e5299_s3126_r10201_p4029',
    'mc16_13TeV.364127.Sherpa_221_NNPDF30NNLO_Zee_MAXHTPTV1000_E_CMS.deriv.DAOD_TOPQ1.e5299_s3126_r10201_p4029',
    'mc16_13TeV.364128.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_TOPQ1.e5307_s3126_r10201_p4029',
    'mc16_13TeV.364129.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_TOPQ1.e5307_s3126_r10201_p4029',
    'mc16_13TeV.364130.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_BFilter.deriv.DAOD_TOPQ1.e5307_s3126_r10201_p4029',
    'mc16_13TeV.364131.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_TOPQ1.e5307_s3126_r10201_p4029',
    'mc16_13TeV.364132.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_TOPQ1.e5307_s3126_r10201_p4029',
    'mc16_13TeV.364133.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_BFilter.deriv.DAOD_TOPQ1.e5307_s3126_r10201_p4029',
    'mc16_13TeV.364134.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_TOPQ1.e5307_s3126_r10201_p4029',
    'mc16_13TeV.364135.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_TOPQ1.e5307_s3126_r10201_p4029',
    'mc16_13TeV.364136.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_BFilter.deriv.DAOD_TOPQ1.e5307_s3126_r10201_p4029',
    'mc16_13TeV.364137.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_TOPQ1.e5307_s3126_r10201_p4029',
    'mc16_13TeV.364138.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_TOPQ1.e5313_s3126_r10201_p4029',
    'mc16_13TeV.364139.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_BFilter.deriv.DAOD_TOPQ1.e5313_s3126_r10201_p4029',
    'mc16_13TeV.364140.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV500_1000.deriv.DAOD_TOPQ1.e5307_s3126_r10201_p4029',
    'mc16_13TeV.364141.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV1000_E_CMS.deriv.DAOD_TOPQ1.e5307_s3126_r10201_p4029',
    'mc16_13TeV.364198.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BVeto.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364199.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV0_70_BFilter.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364200.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV70_280_BVeto.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364201.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV70_280_BFilter.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364202.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV280_E_CMS_BVeto.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364203.Sherpa_221_NN30NNLO_Zmm_Mll10_40_MAXHTPTV280_E_CMS_BFilter.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364204.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV0_70_BVeto.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364205.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV0_70_BFilter.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364206.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV70_280_BVeto.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364207.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV70_280_BFilter.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364208.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV280_E_CMS_BVeto.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364209.Sherpa_221_NN30NNLO_Zee_Mll10_40_MAXHTPTV280_E_CMS_BFilter.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364210.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV0_70_BVeto.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364211.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV0_70_BFilter.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364212.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV70_280_BVeto.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364213.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV70_280_BFilter.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364214.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV280_E_CMS_BVeto.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    'mc16_13TeV.364215.Sherpa_221_NN30NNLO_Ztt_Mll10_40_MAXHTPTV280_E_CMS_BFilter.deriv.DAOD_TOPQ1.e5421_s3126_r10201_p4029',
    ]

TopExamples.grid.Add("wjets_mcd").datasets = [
    'mc16_13TeV.364156.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364157.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364158.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV0_70_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364159.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364160.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364161.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV70_140_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364162.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364163.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364164.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV140_280_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364165.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364166.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364167.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV280_500_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364168.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV500_1000.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364169.Sherpa_221_NNPDF30NNLO_Wmunu_MAXHTPTV1000_E_CMS.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364170.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364171.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364172.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV0_70_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364173.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364174.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364175.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV70_140_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364176.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364177.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364178.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV140_280_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364179.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364180.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364181.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV280_500_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364182.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV500_1000.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364183.Sherpa_221_NNPDF30NNLO_Wenu_MAXHTPTV1000_E_CMS.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364184.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364185.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364186.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV0_70_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364187.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364188.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364189.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV70_140_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364190.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364191.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364192.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV140_280_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364193.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364194.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364195.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV280_500_BFilter.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364196.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV500_1000.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    'mc16_13TeV.364197.Sherpa_221_NNPDF30NNLO_Wtaunu_MAXHTPTV1000_E_CMS.deriv.DAOD_TOPQ1.e5340_s3126_r10201_p4029',
    ]

TopExamples.grid.Add("ttV_mcd").datasets = [
    'mc16_13TeV.410218.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttee.deriv.DAOD_TOPQ1.e5070_s3126_r10201_p4031',
    'mc16_13TeV.410219.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttmumu.deriv.DAOD_TOPQ1.e5070_s3126_r10201_p4031',
    'mc16_13TeV.410220.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_tttautau.deriv.DAOD_TOPQ1.e5070_s3126_r10201_p4031',
    'mc16_13TeV.410155.aMcAtNloPythia8EvtGen_MEN30NLO_A14N23LO_ttW.deriv.DAOD_TOPQ1.e5070_s3126_r10201_p4031',
    'mc16_13TeV.410408.aMcAtNloPythia8EvtGen_tWZ_Ztoll_minDR1.deriv.DAOD_TOPQ1.e6423_s3126_r10201_p4031',
    'mc16_13TeV.412063.aMcAtNloPythia8EvtGen_tllq_NNPDF30_nf4_A14.deriv.DAOD_TOPQ1.e7054_s3126_r10201_p4062'
    ]

# aMC@NLO+Py8, Generator systematic                                                                                                                                     
TopExamples.grid.Add("ttbar_dil_aMCP8_mcd").datasets = [
    'mc16_13TeV.410465.aMcAtNloPy8EvtGen_MEN30NLO_A14N23LO_ttbar_noShWe_dil.deriv.DAOD_TOPQ1.e6762_a875_r10201_p4031',
    ]

# Powheg+Herwig7.0 parton shower systematic                                                                                                                                      
TopExamples.grid.Add("ttbar_dil_PH7old_mcd").datasets = [
    'mc16_13TeV.410558.PowhegHerwig7EvtGen_H7UE_tt_hdamp258p75_704_dil.deriv.DAOD_TOPQ1.e6366_a875_r10201_p4031'
    ]
    
# Powheg+Herwig7.1.3 parton shower systematic                                                                                                                                      
TopExamples.grid.Add("ttbar_dil_PH7_mcd").datasets = [
    'mc16_13TeV.411234.PowhegHerwig7EvtGen_tt_hdamp258p75_713_dil.deriv.DAOD_TOPQ1.e7580_a875_r10201_p4031',
    ]

# aMC@NLO+Herwig7.1.3 parton shower systematic
TopExamples.grid.Add("ttbar_dil_aMCH7_mcd").datasets = [
    'mc16_13TeV.412117.aMcAtNloHerwig7EvtGen_MEN30NLO_ttbar_dil.deriv.DAOD_TOPQ1.e7620_a875_r10201_p4031',
    ]

# "hdamp up" sample
TopExamples.grid.Add("ttbar_dil_hdamp_mcd").datasets = [
    'mc16_13TeV.410482.PhPy8EG_A14_ttbar_hdamp517p5_dil.deriv.DAOD_TOPQ1.e6454_a875_r10201_p4031',
    ]

# Wt DS
TopExamples.grid.Add("sintop_wt_dilep_DS_mcd").datasets = [
    'mc16_13TeV.410654.PowhegPythia8EvtGen_A14_Wt_DS_inclusive_top.deriv.DAOD_TOPQ1.e6552_s3126_r10201_p4031',
    'mc16_13TeV.410655.PowhegPythia8EvtGen_A14_Wt_DS_inclusive_antitop.deriv.DAOD_TOPQ1.e6552_s3126_r10201_p4031',
    ]

# Wt Her7
TopExamples.grid.Add("sintop_wt_dilep_PH7_mcd").datasets = [
    'mc16_13TeV.411038.PowhegHerwig7EvtGen_H7UE_Wt_DR_dilepton_top.deriv.DAOD_TOPQ1.e6702_a875_r10201_p4031',
    'mc16_13TeV.411039.PowhegHerwig7EvtGen_H7UE_Wt_DR_dilepton_antitop.deriv.DAOD_TOPQ1.e6702_a875_r10201_p4031',
    ]

# Wt aMC@NLO+Py8
TopExamples.grid.Add("sintop_wt_dilep_aMCP8_mcd").datasets = [
    'mc16_13TeV.412003.aMcAtNloPythia8EvtGen_HThalfscale_tW_dilepton.deriv.DAOD_TOPQ1.e6817_a875_r10201_p4031',
    ]

TopExamples.grid.Add("zprime400_mcd").datasets = [
    'mc16_13TeV.301322.Pythia8EvtGen_A14NNPDF23LO_zprime400_tt.deriv.DAOD_EXOT4.e4061_s3126_r10201_p3992',
    ]
TopExamples.grid.Add("zprime500_mcd").datasets = [
    'mc16_13TeV.301323.Pythia8EvtGen_A14NNPDF23LO_zprime500_tt.deriv.DAOD_EXOT4.e4061_s3126_r10201_p3992',
    ]
TopExamples.grid.Add("zprime750_mcd").datasets = [
    'mc16_13TeV.301324.Pythia8EvtGen_A14NNPDF23LO_zprime750_tt.deriv.DAOD_EXOT4.e4061_s3126_r10201_p3992',
    ]
TopExamples.grid.Add("zprime1000_mcd").datasets = [
    'mc16_13TeV.301325.Pythia8EvtGen_A14NNPDF23LO_zprime1000_tt.deriv.DAOD_EXOT4.e4061_s3126_r10201_p3992',
    ]
TopExamples.grid.Add("zprime1250_mcd").datasets = [
    'mc16_13TeV.301326.Pythia8EvtGen_A14NNPDF23LO_zprime1250_tt.deriv.DAOD_EXOT4.e4061_s3126_r10201_p3992',
    ]
TopExamples.grid.Add("zprime1500_mcd").datasets = [
    'mc16_13TeV.301327.Pythia8EvtGen_A14NNPDF23LO_zprime1500_tt.deriv.DAOD_EXOT4.e4061_s3126_r10201_p3992',
    ]
TopExamples.grid.Add("zprime1750_mcd").datasets = [
    'mc16_13TeV.301328.Pythia8EvtGen_A14NNPDF23LO_zprime1750_tt.deriv.DAOD_EXOT4.e4061_s3126_r10201_p3992',
    ]
TopExamples.grid.Add("zprime2000_mcd").datasets = [
    'mc16_13TeV.301329.Pythia8EvtGen_A14NNPDF23LO_zprime2000_tt.deriv.DAOD_EXOT4.e4061_s3126_r10201_p3992',
    ]
TopExamples.grid.Add("zprime2250_mcd").datasets = [
    'mc16_13TeV.301330.Pythia8EvtGen_A14NNPDF23LO_zprime2250_tt.deriv.DAOD_EXOT4.e4061_s3126_r10201_p3992',
    ]
TopExamples.grid.Add("zprime2500_mcd").datasets = [
    'mc16_13TeV.301331.Pythia8EvtGen_A14NNPDF23LO_zprime2500_tt.deriv.DAOD_EXOT4.e4061_s3126_r10201_p3992',
    ]
TopExamples.grid.Add("zprime2750_mcd").datasets = [
    'mc16_13TeV.301332.Pythia8EvtGen_A14NNPDF23LO_zprime2750_tt.deriv.DAOD_EXOT4.e4061_s3126_r10201_p3992',
    ]
TopExamples.grid.Add("zprime3000_mcd").datasets = [
    'mc16_13TeV.301333.Pythia8EvtGen_A14NNPDF23LO_zprime3000_tt.deriv.DAOD_EXOT4.e3723_s3126_r10201_p3992',
    ]
TopExamples.grid.Add("zprime4000_mcd").datasets = [
    'mc16_13TeV.301334.Pythia8EvtGen_A14NNPDF23LO_zprime4000_tt.deriv.DAOD_EXOT4.e3723_s3126_r10201_p3992',
    ]
TopExamples.grid.Add("zprime5000_mcd").datasets = [
    'mc16_13TeV.301335.Pythia8EvtGen_A14NNPDF23LO_zprime5000_tt.deriv.DAOD_EXOT4.e3723_s3126_r10201_p3992',
    ]

# need to add new derivations as soon as they are available
TopExamples.grid.Add("kkg500_mcd").datasets = [
    #'mc16_13TeV.307522.Pythia8EvtGen_A14NNPDF23LO_KKgluon500_Width30_tt.deriv.DAOD_EXOT4.e5616_e5984_s3126_r10201_r9315_p3992',
    ]
TopExamples.grid.Add("kkg1000_mcd").datasets = [
    #'mc16_13TeV.307523.Pythia8EvtGen_A14NNPDF23LO_KKgluon1000_Width30_tt.deriv.DAOD_EXOT4.e5616_e5984_s3126_r10201_r9315_p3992',
    ]
TopExamples.grid.Add("kkg1500_mcd").datasets = [
    #'mc16_13TeV.307524.Pythia8EvtGen_A14NNPDF23LO_KKgluon1500_Width30_tt.deriv.DAOD_EXOT4.e5616_e5984_s3126_r10201_r9315_p3992',
    ]
TopExamples.grid.Add("kkg2000_mcd").datasets = [
    #'mc16_13TeV.307527.Pythia8EvtGen_A14NNPDF23LO_KKgluon2000_Width30_tt.deriv.DAOD_EXOT4.e5616_e5984_s3126_r10201_r9315_p3992',
    ]
TopExamples.grid.Add("kkg2500_mcd").datasets = [
    #'mc16_13TeV.307528.Pythia8EvtGen_A14NNPDF23LO_KKgluon2500_Width30_tt.deriv.DAOD_EXOT4.e5616_e5984_s3126_r10201_r9315_p3992',
    ]
TopExamples.grid.Add("kkg3000_mcd").datasets = [
    #'mc16_13TeV.307529.Pythia8EvtGen_A14NNPDF23LO_KKgluon3000_Width30_tt.deriv.DAOD_EXOT4.e5616_e5984_s3126_r10201_r9315_p3992',
    ]
TopExamples.grid.Add("kkg3500_mcd").datasets = [
    #'mc16_13TeV.307530.Pythia8EvtGen_A14NNPDF23LO_KKgluon3500_Width30_tt.deriv.DAOD_EXOT4.e5616_e5984_s3126_r10201_r9315_p3992',
    ]
TopExamples.grid.Add("kkg4000_mcd").datasets = [
    #'mc16_13TeV.307531.Pythia8EvtGen_A14NNPDF23LO_KKgluon4000_Width30_tt.deriv.DAOD_EXOT4.e5616_e5984_s3126_r10201_r9315_p3992',
    ]
TopExamples.grid.Add("kkg4500_mcd").datasets = [
    #'mc16_13TeV.307532.Pythia8EvtGen_A14NNPDF23LO_KKgluon4500_Width30_tt.deriv.DAOD_EXOT4.e5616_e5984_s3126_r10201_r9315_p3992',
    ]
TopExamples.grid.Add("kkg5000_mcd").datasets = [
    #'mc16_13TeV.307533.Pythia8EvtGen_A14NNPDF23LO_KKgluon5000_Width30_tt.deriv.DAOD_EXOT4.e5616_e5984_s3126_r10201_r9315_p3992',
    ]

TopExamples.grid.Add("grav400_mcd").datasets = [
    'mc16_13TeV.305012.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_tt_c10_m0400.deriv.DAOD_EXOT4.e4859_s3126_r10201_p4062',
    ]
TopExamples.grid.Add("grav500_mcd").datasets = [
    'mc16_13TeV.305013.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_tt_c10_m0500.deriv.DAOD_EXOT4.e4859_s3126_r10201_p4062',
    ]
TopExamples.grid.Add("grav750_mcd").datasets = [
    'mc16_13TeV.305014.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_tt_c10_m0750.deriv.DAOD_EXOT4.e4859_s3126_r10201_p4062',
    ]
TopExamples.grid.Add("grav1000_mcd").datasets = [
    'mc16_13TeV.305015.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_tt_c10_m1000.deriv.DAOD_EXOT4.e4859_s3126_r10201_p4062',
    ]
TopExamples.grid.Add("grav2000_mcd").datasets = [
    'mc16_13TeV.305016.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_tt_c10_m2000.deriv.DAOD_EXOT4.e4859_s3126_r10201_p4062',
    ]
TopExamples.grid.Add("grav3000_mcd").datasets = [
    'mc16_13TeV.305017.MadGraphPythia8EvtGen_A14NNPDF23LO_RS_G_tt_c10_m3000.deriv.DAOD_EXOT4.e4859_s3126_r10201_p4062',
    ]

TopExamples.grid.Add("hvt1000_mcd").datasets = [
    'mc16_13TeV.310490.MadGraphPythia8EvtGen_A14NNPDF23LO_HVT_Agv1_Vztt_alll_m1000.deriv.DAOD_EXOT4.e6916_s3126_r10201_p4062',
    ]
TopExamples.grid.Add("hvt4000_mcd").datasets = [
    'mc16_13TeV.310491.MadGraphPythia8EvtGen_A14NNPDF23LO_HVT_Agv1_Vztt_alll_m4000.deriv.DAOD_EXOT4.e6916_s3126_r10201_p4062',
    ]
TopExamples.grid.Add("hvt8000_mcd").datasets = [
    'mc16_13TeV.310492.MadGraphPythia8EvtGen_A14NNPDF23LO_HVT_Agv1_Vztt_alll_m8000.deriv.DAOD_EXOT4.e6916_s3126_r10201_p4062',
    ]
