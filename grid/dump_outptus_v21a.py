#!/usr/bin/env python
import getpass
import os
import TopExamples.grid
import Data_v21
import MC16a_v21
#import MC16d_v21
#import MC16e_v21

# Configuration
config = TopExamples.grid.Config()
config.gridUsername  = 'pinamont'
#config.suffix        = 'V21a'
#config.suffix        = 'V21a-bis'
config.suffix        = 'V21a-syst'

names = [
         # Data
         #'data15',
         #'data16',
         #'data17',
         #'data18',
         # Nominal ttbar
         #'ttbar_nah_mca',                                                                                                                                             
         'ttbar_dil_mca',
         # Nominal single top
         #'sintop_mca',
         #'sintop_wt_mca',
         #'sintop_wt_dilep_mca',
         # Nominal other bkg
         #'diboson_mca',
         #'zjets_mca',
         #'wjets_mca',
         #'ttV_mca',
         # Alternative ttbar
         #'ttbar_dil_aMCP8_mca',
         #'ttbar_dil_PH7old_mca',
         #'ttbar_dil_aMCH7_mca',
         #'ttbar_dil_PH7_mca',
         #'ttbar_dil_hdamp_mca',
         # Alternative single top
         #'sintop_wt_dilep_DS_mca',
         #'sintop_wt_dilep_PH7_mca',
         #'sintop_wt_dilep_aMCP8_mca',
         # Singal
         #'zprime400_mca',
         #'zprime500_mca',
         #'zprime750_mca',
         #'zprime1000_mca',
         #'zprime1250_mca',
         #'zprime1500_mca',
         #'zprime1750_mca',
         #'zprime2000_mca',
         #'zprime2250_mca',
         #'zprime2500_mca',
         #'zprime2750_mca',
         #'zprime3000_mca',
         #'zprime4000_mca',
         #'zprime5000_mca',
         ##'mc16a_KKgluon_tt',   # missing proper derivation tag ?
         ##'mc16a_graviton_tt',  # crashing...
         ##'mc16a_HVT_tt',       # missing x-sections
         ]
samples = TopExamples.grid.Samples(names)

os.system("mkdir "+config.suffix)

for sample in samples:
    print "Creating file "+sample.name+".txt"
    text_file = open(config.suffix+"/"+sample.name+".txt", "w")
    for ds in sample.datasets:
        ds_short = TopExamples.grid.basicInDSNameShortener('',ds)
        text_file.write("user."+config.gridUsername+"."+ds_short+"."+config.suffix+"_output_root\n")
    text_file.close()
