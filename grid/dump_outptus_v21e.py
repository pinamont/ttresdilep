#!/usr/bin/env python
import getpass
import os
import TopExamples.grid
import Data_v21
#import MC16a_v21
#import MC16d_v21
import MC16e_v21

# Configuration
config = TopExamples.grid.Config()
config.gridUsername  = 'pinamont'
#config.suffix        = 'V21e'
config.suffix        = 'V21e-bis'
#config.suffix        = 'V21e-syst'

names = [
         # Data
         #'data15',
         #'data16',
         #'data17',
         'data18',
         # Nominal ttbar
         #'ttbar_nah_mce',                                                                                                                                             
         #'ttbar_dil_mce',
         ## Nominal single top
         #'sintop_mce',
         #'sintop_wt_mce',
         #'sintop_wt_dilep_mce',
         ## Nominal other bkg
         #'diboson_mce',
         #'zjets_mce',
         #'wjets_mce',
         #'ttV_mce',
         ## Alternative ttbar
         #'ttbar_dil_aMCP8_mce',
         #'ttbar_dil_PH7old_mce',
         #'ttbar_dil_aMCH7_mce',
         #'ttbar_dil_PH7_mce',
         #'ttbar_dil_hdamp_mce',
         ## Alternative single top
         #'sintop_wt_dilep_DS_mce',
         #'sintop_wt_dilep_PH7_mce',
         #'sintop_wt_dilep_aMCP8_mce',
         ## Singal
         #'zprime400_mce',
         #'zprime500_mce',
         #'zprime750_mce',
         #'zprime1000_mce',
         #'zprime1250_mce',
         #'zprime1500_mce',
         #'zprime1750_mce',
         #'zprime2000_mce',
         #'zprime2250_mce',
         #'zprime2500_mce',
         #'zprime2750_mce',
         #'zprime3000_mce',
         #'zprime4000_mce',
         #'zprime5000_mce',
         ##'mc16a_KKgluon_tt',   # missing proper derivation tag ?
         ##'mc16a_graviton_tt',  # crashing...
         ##'mc16a_HVT_tt',       # missing x-sections
         ]
samples = TopExamples.grid.Samples(names)

os.system("mkdir "+config.suffix)

for sample in samples:
    print "Creating file "+sample.name+".txt"
    text_file = open(config.suffix+"/"+sample.name+".txt", "w")
    for ds in sample.datasets:
        ds_short = TopExamples.grid.basicInDSNameShortener('',ds)
        text_file.write("user."+config.gridUsername+"."+ds_short+"."+config.suffix+"_output_root\n")
    text_file.close()
