#!/usr/bin/env python
import getpass
import os
import TopExamples.grid
import Data_v21
#import MC16a_v21
#import MC16d_v21
import MC16e_v21

# Configuration
config = TopExamples.grid.Config()
config.CMake         = True
config.code          = 'top-xaod'
#config.settingsFile  = 'ttres-dilep-cuts-mc16a-noSyst.txt'
#config.settingsFile  = 'ttres-dilep-cuts-mc16d-allSyst.txt'
#config.settingsFile  = 'ttres-dilep-cuts-mc16e-allSyst.txt'
#config.settingsFile  = 'ttres-dilep-cuts-mc16a-noSyst.txt'
#config.settingsFile  = 'ttres-dilep-cuts-mc16d-noSyst.txt'
config.settingsFile  = 'ttres-dilep-cuts-mc16e-noSyst.txt'
#config.gridUsername  = 'madesant'
#config.gridUsername  = 'mfaraj'
config.gridUsername  = 'pinamont'
#config.suffix        = 'V21e'
config.suffix        = 'V21e-bis'
#config.suffix        = 'V21e-syst'
#config.excludedSites = ''
config.excludedSites = 'ANALY_SCINET'
#config.excludedSites = 'INFN-NAPOLI'
config.noSubmit      = False
config.mergeType     = 'None' #'None', 'Default' or 'xAOD'
#config.mergeType     = 'Default'
#config.memory        = '1000'
config.destSE        = 'INFN-TRIESTE_LOCALGROUPDISK'
config.maxNFilesPerJob  = '5'
#config.maxNFilesPerJob  = '1'

# -- additional options
# to use when resubmitting 'broken' tasks
#config.otherOptions  = '--allowTaskDuplication'
 
names = [
         ## Data
         #'data15',
         #'data16',
         #'data17',
         'data18',
         ## Nominal ttbar
         #'ttbar_nah_mce',                                                                                                                                             
         #'ttbar_dil_mce',
         ## Nominal single top
         #'sintop_mce',
         #'sintop_wt_mce',
         #'sintop_wt_dilep_mce',
         ## Nominal other bkg
         #'diboson_mce',
         #'zjets_mce',
         #'wjets_mce',
         #'ttV_mce',
         ## Alternative ttbar
         #'ttbar_dil_aMCP8_mce',
         #'ttbar_dil_PH7old_mce',
         #'ttbar_dil_aMCH7_mce',
         #'ttbar_dil_PH7_mce',
         #'ttbar_dil_hdamp_mce',
         ## Alternative single top
         #'sintop_wt_dilep_DS_mce',
         #'sintop_wt_dilep_PH7_mce',
         #'sintop_wt_dilep_aMCP8_mce',
         ## Singal
         #'zprime400_mce',
         #'zprime500_mce',
         #'zprime750_mce',
         #'zprime1000_mce',
         #'zprime1250_mce',
         #'zprime1500_mce',
         #'zprime1750_mce',
         #'zprime2000_mce',
         #'zprime2250_mce',
         #'zprime2500_mce',
         #'zprime2750_mce',
         #'zprime3000_mce',
         #'zprime4000_mce',
         #'zprime5000_mce',
         ##'mc16a_KKgluon_tt',   # missing proper derivation tag ?
         ##'mc16a_graviton_tt',  # crashing...
         #'grav400_mce',
         #'grav500_mce',
         #'grav750_mce',
         #'grav1000_mce',
         #'grav2000_mce',
         #'grav3000_mce',
         ##'mc16a_HVT_tt',       # missing x-sections
         ]
samples = TopExamples.grid.Samples(names)

# copy the configuration file from the run directory
os.system("scp ../run/"+config.settingsFile+" .")

# Submit Jobs
#TopExamples.ami.check_sample_status(samples)
TopExamples.grid.submit(config, samples)
