
import TopExamples.grid
import TopExamples.ami

DATA15 = [
    'data15_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp15_v01_p4030',
    'data15_13TeV.periodAllYear.debugrec_hlt.PhysCont.DAOD_TOPQ1.grp15_v01_p4032',
          ]

DATA16 = [
    'data16_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp16_v01_p4030',
    'data16_13TeV.periodAllYear.debugrec_hlt.PhysCont.DAOD_TOPQ1.grp16_v01_p4032',
          ]

DATA17 = [
    'data17_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp17_v01_p4030',
    'data17_13TeV.periodAllYear.debugrec_hlt.PhysCont.DAOD_TOPQ1.grp17_v01_p4032',
          ]

DATA18 = [
    'data18_13TeV.periodAllYear.physics_Main.PhysCont.DAOD_TOPQ1.grp18_v01_p4030',
    'data18_13TeV.periodAllYear.debugrec_hlt.PhysCont.DAOD_TOPQ1.grp18_v01_p4032',
          ]

# Period containers - preferred choice
TopExamples.grid.Add('data15').datasets = [i for i in DATA15]
TopExamples.grid.Add('data16').datasets = [i for i in DATA16]
TopExamples.grid.Add('data17').datasets = [i for i in DATA17]
TopExamples.grid.Add('data18').datasets = [i for i in DATA18]
