#!/usr/bin/env python
import getpass
import os
import TopExamples.grid
import Data_v21
import MC16a_v21
#import MC16d_v21
#import MC16e_v21

names = [
         # Data
         'data15',
         'data16',
         ]
names+= [
         ## Nominal ttbar
         'ttbar_nah_mca',                                                                                                                                             
         'ttbar_dil_mca',
         # Nominal single top
         'sintop_mca',
         'sintop_wt_mca',
         'sintop_wt_dilep_mca',
         # Nominal other bkg
         'diboson_mca',
         'zjets_mca',
         'wjets_mca',
         'ttV_mca',
         # Alternative ttbar
         'ttbar_dil_aMCP8_mca',
         'ttbar_dil_PH7old_mca',
         'ttbar_dil_aMCH7_mca',
         'ttbar_dil_PH7_mca',
         'ttbar_dil_hdamp_mca',
         # Alternative single top
         'sintop_wt_dilep_DS_mca',
         'sintop_wt_dilep_PH7_mca',
         'sintop_wt_dilep_aMCP8_mca',
         ## Singal
         'zprime400_mca',
         'zprime500_mca',
         'zprime750_mca',
         'zprime1000_mca',
         'zprime1250_mca',
         'zprime1500_mca',
         'zprime1750_mca',
         'zprime2000_mca',
         'zprime2250_mca',
         'zprime2500_mca',
         'zprime2750_mca',
         'zprime3000_mca',
         'zprime4000_mca',
         'zprime5000_mca',
         ]
samples = TopExamples.grid.Samples(names)

print "**********************************************************"
for sample in samples:
    print "------------------------------------"
    print "Checking "+sample.name
    print "------------------------------------"
    for ds in sample.datasets:
        print ds
        os.system("rucio ls "+ds+" | grep CONTAINER")
        
print "------------------------------------"
