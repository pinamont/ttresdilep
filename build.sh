version=21.2.89
if [[ $1 != "" ]]; then version=$1; fi

home=`pwd`

echo "--------------------"
echo "==> Running  source settings.sh ..."
echo "--------------------"
source settings.sh

rm -rf build

# for online & offline running!
echo "--------------------"
echo "==> Running  asetup ..."
echo "--------------------"
cd source
if [[ ${version} == 21.2.89 ]]
then
    asetup "AnalysisTop,${version},here"
else
    asetup "AnalysisBase,${version},here"
fi
cd ${home}
echo "--------------------"
echo "==> Setting up submodeules ..."
echo "--------------------"
git submodule init
git submodule update
# hack, in case submodeule update doesn't do its job...
cd source/TQPTtbarResDilep/; git checkout master; git pull; cd ../..
cd source/TopOffline/; git checkout master; git pull; cd ../..
# hack, to have TopOffile working fine
rm source/TopOffline/CMakeLists.txt # FIXME (In TopOffline)
echo "--------------------"
echo "==> Running  source copy_packages.sh ..."
echo "--------------------"
source copy_packages.sh
echo "--------------------"
echo "==> Compiling ..."
echo "--------------------"
mkdir build
cd build
cmake ../source && make
cd ${home} 
echo "--------------------"
echo "==> Running  source build/*/setup.sh ..."
echo "--------------------"
if [[ ${AnalysisTop_PLATFORM} != "" ]]
then
    source build/${AnalysisTop_PLATFORM}/setup.sh
else
    source build/${AnalysisBase_PLATFORM}/setup.sh
fi
source build/*/setup.sh
echo "--------------------"
echo "==> Exporting LD_LIBRARY_PATH ..."
echo "--------------------"
if [[ ${AnalysisTop_PLATFORM} != "" ]]
then
    export LD_LIBRARY_PATH=`pwd`/build/${AnalysisTop_PLATFORM}/lib:$LD_LIBRARY_PATH
else
    export LD_LIBRARY_PATH=`pwd`/build/${AnalysisBase_PLATFORM}/lib:$LD_LIBRARY_PATH
fi
