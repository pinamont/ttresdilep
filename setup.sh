home=`pwd`

source settings.sh

# for online & offline running!
cd source
asetup  --restore
cd ..
if [[ ${AnalysisTop_PLATFORM} != "" ]]
then
    source build/${AnalysisTop_PLATFORM}/setup.sh
else
    source build/${AnalysisBase_PLATFORM}/setup.sh
fi
source build/*/setup.sh
if [[ ${AnalysisTop_PLATFORM} != "" ]]
then
    export LD_LIBRARY_PATH=`pwd`/build/${AnalysisTop_PLATFORM}/lib:$LD_LIBRARY_PATH
else
    export LD_LIBRARY_PATH=`pwd`/build/${AnalysisBase_PLATFORM}/lib:$LD_LIBRARY_PATH
fi
