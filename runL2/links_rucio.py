#Creating symlinks for files stored with rucio directions
#by Elena Oliver 2013
#modified by Rachik Soualah and Michele Pinamonti 2015
# Dec 2016: modified by Leonid and Michele to use rucio, adapted the naming for use of HT+X samples
# June 2017: adding DAOD infos to filenames, needed for weight_normalise calculation for ttbar systematics (G.)
 
import commands
import os
import sys

# Get the arguments
dataset=sys.argv[1]    # something like user.rsoualah.mc15_13TeV.187172.PowhegPythia8_WpZ_e2tau_mll3p804_DiLeptonFilter.merge.DAOD_TOPQ1_p2372_88_V1_output.root
sample=sys.argv[2]     # something like singleTop
directory=sys.argv[3]  # something like /gpfs/atlas/public/ttH_ntup2015/flatNtup_TTHBB_grid_v1.0/

# Print what you are doing
print "--------------------------------"
print "  Checking out dataset ",dataset
print "  Belonging to sample ",sample
print "  Adding it to ",directory

# Check if the directory exists, create it and go to in
if not os.path.exists(directory):
        os.makedirs(directory)
os.chdir(directory)
 
# -> Change the word after grep according to your site Storage Element
output=commands.getoutput("rucio list-file-replicas --rse INFN-TRIESTE_LOCALGROUPDISK " +dataset+"/ | grep srm | sed 's/^.*SFN=//' | sed -e 's/|//' | sed -e 's/  *$//' " )
# sed 's/^.*SFN=//' -> get only those lines after string "SFN="
# sed -e 's/|//'    -> remove the string "|"
# sed -e 's/  *$//' -> remove whitespace  

print output


files=[]

if output != "":
    files=output.split("\n")
else:
    print "    WARNING:  NO FILES FOUND ..."

# Loop on files and create a symlink for each of them 
for i in files:
    infile ="/gpfs/grid/srm" + i
    print "File: ",infile
    divisions=i.split('/')
    # Get sample/run number
    runNumber=dataset.split('.')[3]
    namerunNumber=dataset.split('.')[4]
    derivation=dataset.split('.')[5]
    if dataset.find("SM4t") != -1:
         runNumber=dataset.split('.')[2]
         namerunNumber=dataset.split('.')[3]
         derivation=dataset.split('.')[4]
         derivation=derivation.split('_')[1]
    campaign=""
    if dataset.find("data") != -1:
        derivation=dataset.split('.')[4]
        systematics=""
        if dataset.find("_fakes_") != -1:
              systematics=".fakes"        
    else:
        systematics=".nom"
        if dataset.find("_sys_") != -1:
              systematics=".sys"
        if dataset.find("_jet_") != -1:
              systematics=".jet"
        if dataset.find("_lrj_") != -1:
              systematics=".lrj"
        campaign=".unknw"
        if dataset.find("r10724") != -1:
              campaign=".mc16e"
        if dataset.find("r10201") != -1:
              campaign=".mc16d"
        if dataset.find("r9364") != -1:
              campaign=".mc16a"
        if dataset.find("r9781") != -1:
              campaign=".mc16c"
   # Create output file name
    outname=directory+"/"+sample 
    outname+="."+runNumber+"."+derivation+campaign+systematics + "."
    outname+=infile.split('.')[2]+infile.split('.')[3]
    outname+=".root"
    print "      Linked to new file ",outname
    os.symlink(infile, outname)

# Go back to the initial directory
os.chdir('..')
print "--------------------------------"
