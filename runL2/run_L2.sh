dataSamples=""
# dataSamples+=" data15"
# dataSamples+=" data16"
# dataSamples+=" data17"
# dataSamples+=" data18"

mcSamples=""
#
mcSamples+=" ttbar_dil_mca"
# mcSamples+=" ttbar_nah_mca"
# mcSamples+=" wjets_mca"
# mcSamples+=" zjets_mca"
# mcSamples+=" sintop_mca"
# mcSamples+=" sintop_wt_mca"
# mcSamples+=" sintop_wt_dilep_mca"
# mcSamples+=" diboson_mca"
# mcSamples+=" ttV_mca"
# mcSamples+=" zprime500_mca"
# mcSamples+=" zprime750_mca"
# mcSamples+=" zprime1000_mca"
# mcSamples+=" zprime2000_mca"
# mcSamples+=" zprime3000_mca"
# mcSamples+=" zprime4000_mca"
# mcSamples+=" zprime5000_mca"
# #
mcSamples+=" ttbar_dil_mcd"
# mcSamples+=" ttbar_nah_mcd"
# mcSamples+=" wjets_mcd"
# mcSamples+=" zjets_mcd"
# mcSamples+=" sintop_mcd"
# mcSamples+=" sintop_wt_mcd"
# mcSamples+=" sintop_wt_dilep_mcd"
# mcSamples+=" diboson_mcd"
# mcSamples+=" ttV_mcd"
# mcSamples+=" zprime500_mcd"
# mcSamples+=" zprime750_mcd"
# mcSamples+=" zprime1000_mcd"
# mcSamples+=" zprime2000_mcd"
# mcSamples+=" zprime3000_mcd"
# mcSamples+=" zprime4000_mcd"
# mcSamples+=" zprime5000_mcd"
# #
mcSamples+=" ttbar_dil_mce"
# mcSamples+=" ttbar_nah_mce"
# mcSamples+=" wjets_mce"
# mcSamples+=" zjets_mce"
# mcSamples+=" sintop_mce"
# mcSamples+=" sintop_wt_mce"
# mcSamples+=" sintop_wt_dilep_mce"
# mcSamples+=" diboson_mce"
# mcSamples+=" ttV_mce"
# mcSamples+=" zprime500_mce"
# mcSamples+=" zprime750_mce"
# mcSamples+=" zprime1000_mce"
# mcSamples+=" zprime2000_mce"
# mcSamples+=" zprime3000_mce"
# mcSamples+=" zprime4000_mce"
# mcSamples+=" zprime5000_mce"

configs=""
# configs+=" offline_configuration.txt"
configs+=" offline_configuration_ee.txt"
configs+=" offline_configuration_mumu.txt"
configs+=" offline_configuration_emu.txt"

queue="ts_atlas"
# queue="normal_io"

# outDir="/gpfs/atlas/public/ttRes_ntuples/V21_L2/"
outDir="/eos/infnts/atlas/public/ttRes_L2ntuples/V21-syst/"

systematics=""
# systematics+=" nominal"
systematics+=" CategoryReduction_JET_BJES_Response__1down CategoryReduction_JET_BJES_Response__1up CategoryReduction_JET_CombMass_Baseline__1down CategoryReduction_JET_CombMass_Baseline__1up CategoryReduction_JET_CombMass_Modelling__1down CategoryReduction_JET_CombMass_Modelling__1up CategoryReduction_JET_CombMass_TotalStat__1down CategoryReduction_JET_CombMass_TotalStat__1up CategoryReduction_JET_CombMass_Tracking1__1down CategoryReduction_JET_CombMass_Tracking1__1up CategoryReduction_JET_CombMass_Tracking2__1down CategoryReduction_JET_CombMass_Tracking2__1up CategoryReduction_JET_CombMass_Tracking3__1down CategoryReduction_JET_CombMass_Tracking3__1up CategoryReduction_JET_EffectiveNP_Detector1__1down CategoryReduction_JET_EffectiveNP_Detector1__1up CategoryReduction_JET_EffectiveNP_Detector2__1down CategoryReduction_JET_EffectiveNP_Detector2__1up CategoryReduction_JET_EffectiveNP_Mixed1__1down CategoryReduction_JET_EffectiveNP_Mixed1__1up CategoryReduction_JET_EffectiveNP_Mixed2__1down CategoryReduction_JET_EffectiveNP_Mixed2__1up CategoryReduction_JET_EffectiveNP_Mixed3__1down CategoryReduction_JET_EffectiveNP_Mixed3__1up CategoryReduction_JET_EffectiveNP_Modelling1__1down CategoryReduction_JET_EffectiveNP_Modelling1__1up CategoryReduction_JET_EffectiveNP_Modelling2__1down CategoryReduction_JET_EffectiveNP_Modelling2__1up CategoryReduction_JET_EffectiveNP_Modelling3__1down CategoryReduction_JET_EffectiveNP_Modelling3__1up CategoryReduction_JET_EffectiveNP_Modelling4__1down CategoryReduction_JET_EffectiveNP_Modelling4__1up CategoryReduction_JET_EffectiveNP_R10_Detector1__1down CategoryReduction_JET_EffectiveNP_R10_Detector1__1up CategoryReduction_JET_EffectiveNP_R10_Detector2__1down CategoryReduction_JET_EffectiveNP_R10_Detector2__1up CategoryReduction_JET_EffectiveNP_R10_Mixed1__1down CategoryReduction_JET_EffectiveNP_R10_Mixed1__1up CategoryReduction_JET_EffectiveNP_R10_Mixed2__1down CategoryReduction_JET_EffectiveNP_R10_Mixed2__1up CategoryReduction_JET_EffectiveNP_R10_Mixed3__1down CategoryReduction_JET_EffectiveNP_R10_Mixed3__1up CategoryReduction_JET_EffectiveNP_R10_Mixed4__1down CategoryReduction_JET_EffectiveNP_R10_Mixed4__1up CategoryReduction_JET_EffectiveNP_R10_Modelling1__1down CategoryReduction_JET_EffectiveNP_R10_Modelling1__1up CategoryReduction_JET_EffectiveNP_R10_Modelling2__1down CategoryReduction_JET_EffectiveNP_R10_Modelling2__1up CategoryReduction_JET_EffectiveNP_R10_Modelling3__1down CategoryReduction_JET_EffectiveNP_R10_Modelling3__1up CategoryReduction_JET_EffectiveNP_R10_Modelling4__1down CategoryReduction_JET_EffectiveNP_R10_Modelling4__1up CategoryReduction_JET_EffectiveNP_R10_Statistical1__1down CategoryReduction_JET_EffectiveNP_R10_Statistical1__1up CategoryReduction_JET_EffectiveNP_R10_Statistical2__1down CategoryReduction_JET_EffectiveNP_R10_Statistical2__1up CategoryReduction_JET_EffectiveNP_R10_Statistical3__1down CategoryReduction_JET_EffectiveNP_R10_Statistical3__1up CategoryReduction_JET_EffectiveNP_R10_Statistical4__1down CategoryReduction_JET_EffectiveNP_R10_Statistical4__1up CategoryReduction_JET_EffectiveNP_R10_Statistical5__1down CategoryReduction_JET_EffectiveNP_R10_Statistical5__1up CategoryReduction_JET_EffectiveNP_R10_Statistical6__1down CategoryReduction_JET_EffectiveNP_R10_Statistical6__1up CategoryReduction_JET_EffectiveNP_Statistical1__1down CategoryReduction_JET_EffectiveNP_Statistical1__1up CategoryReduction_JET_EffectiveNP_Statistical2__1down CategoryReduction_JET_EffectiveNP_Statistical2__1up CategoryReduction_JET_EffectiveNP_Statistical3__1down CategoryReduction_JET_EffectiveNP_Statistical3__1up CategoryReduction_JET_EffectiveNP_Statistical4__1down CategoryReduction_JET_EffectiveNP_Statistical4__1up CategoryReduction_JET_EffectiveNP_Statistical5__1down CategoryReduction_JET_EffectiveNP_Statistical5__1up CategoryReduction_JET_EffectiveNP_Statistical6__1down CategoryReduction_JET_EffectiveNP_Statistical6__1up CategoryReduction_JET_EtaIntercalibration_Modelling__1down CategoryReduction_JET_EtaIntercalibration_Modelling__1up CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data__1down CategoryReduction_JET_EtaIntercalibration_NonClosure_2018data__1up CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1down CategoryReduction_JET_EtaIntercalibration_NonClosure_highE__1up CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1down CategoryReduction_JET_EtaIntercalibration_NonClosure_negEta__1up CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1down CategoryReduction_JET_EtaIntercalibration_NonClosure_posEta__1up CategoryReduction_JET_EtaIntercalibration_R10_TotalStat__1down CategoryReduction_JET_EtaIntercalibration_R10_TotalStat__1up CategoryReduction_JET_EtaIntercalibration_TotalStat__1down CategoryReduction_JET_EtaIntercalibration_TotalStat__1up CategoryReduction_JET_Flavor_Composition__1down CategoryReduction_JET_Flavor_Composition__1up CategoryReduction_JET_Flavor_Response__1down CategoryReduction_JET_Flavor_Response__1up  CategoryReduction_JET_Pileup_OffsetMu__1down CategoryReduction_JET_Pileup_OffsetMu__1up CategoryReduction_JET_Pileup_OffsetNPV__1down CategoryReduction_JET_Pileup_OffsetNPV__1up CategoryReduction_JET_Pileup_PtTerm__1down CategoryReduction_JET_Pileup_PtTerm__1up CategoryReduction_JET_Pileup_RhoTopology__1down CategoryReduction_JET_Pileup_RhoTopology__1up CategoryReduction_JET_PunchThrough_MC16__1down CategoryReduction_JET_PunchThrough_MC16__1up CategoryReduction_JET_SingleParticle_HighPt__1down CategoryReduction_JET_SingleParticle_HighPt__1up"
systematics+=" CategoryReduction_JET_JER_DataVsMC_MC16__1down CategoryReduction_JET_JER_DataVsMC_MC16__1up CategoryReduction_JET_JER_EffectiveNP_1__1down CategoryReduction_JET_JER_EffectiveNP_1__1up CategoryReduction_JET_JER_EffectiveNP_2__1down CategoryReduction_JET_JER_EffectiveNP_2__1up CategoryReduction_JET_JER_EffectiveNP_3__1down CategoryReduction_JET_JER_EffectiveNP_3__1up CategoryReduction_JET_JER_EffectiveNP_4__1down CategoryReduction_JET_JER_EffectiveNP_4__1up CategoryReduction_JET_JER_EffectiveNP_5__1down CategoryReduction_JET_JER_EffectiveNP_5__1up CategoryReduction_JET_JER_EffectiveNP_6__1down CategoryReduction_JET_JER_EffectiveNP_6__1up CategoryReduction_JET_JER_EffectiveNP_7restTerm__1down CategoryReduction_JET_JER_EffectiveNP_7restTerm__1up"
systematics+=" CategoryReduction_JET_LargeR_TopologyUncertainty_V__1down CategoryReduction_JET_LargeR_TopologyUncertainty_V__1up CategoryReduction_JET_LargeR_TopologyUncertainty_top__1down CategoryReduction_JET_LargeR_TopologyUncertainty_top__1up"
systematics+=" CategoryReduction_JET_MassRes_Hbb_comb__1down CategoryReduction_JET_MassRes_Hbb_comb__1up CategoryReduction_JET_MassRes_Top_comb__1down CategoryReduction_JET_MassRes_Top_comb__1up CategoryReduction_JET_MassRes_WZ_comb__1down CategoryReduction_JET_MassRes_WZ_comb__1up"
systematics+=" EG_RESOLUTION_ALL__1down EG_RESOLUTION_ALL__1up EG_SCALE_AF2__1down EG_SCALE_AF2__1up EG_SCALE_ALL__1down EG_SCALE_ALL__1up MET_SoftTrk_ResoPara MET_SoftTrk_ResoPerp MET_SoftTrk_ScaleDown MET_SoftTrk_ScaleUp MUON_ID__1down MUON_ID__1up MUON_MS__1down MUON_MS__1up MUON_SAGITTA_RESBIAS__1down MUON_SAGITTA_RESBIAS__1up MUON_SAGITTA_RHO__1down MUON_SAGITTA_RHO__1up MUON_SCALE__1down MUON_SCALE__1up"

# inputDir="inputs/"
inputDir="/gpfs/atlas/pinamont/ttResDilep-21.2.115/runL2/inputs/"


# --- --- --- #

here=`pwd`
mkdir ${outDir}

for config in ${configs}
do

    for sample in ${dataSamples}
    do
        errLog="${here}/logs/err/${config}_${sample}.err"
        outLog="${here}/logs/out/${config}_${sample}.out"
        rm ${errLog} ${outLog}
        bsub -q ${queue} -e ${errLog} -o ${outLog} ./job-top-offline-farmts.job "${config} inputs/${sample}.txt -o OutputDirectory ${outDir} SampleName ${sample} SampleType data"
    done

    for sample in ${mcSamples}
    do
        for syst in ${systematics}
        do
            errLog="${here}/logs/err/${config}_${sample}.err"
            outLog="${here}/logs/out/${config}_${sample}.out"
            rm ${errLog} ${outLog}
            systSuf=""
            if [[ ${syst} != "nominal" ]]
            then
                systSuf="__"${syst}
            fi
            bsub -q ${queue} -e ${errLog} -o ${outLog} ./job-top-offline-farmts.job "${config} inputs/${sample}.txt -o OutputDirectory ${outDir} SampleName ${sample}${systSuf} Systematics ${syst}"
        done
    done

done
