# outDir=/gpfs/atlas/public/ttRes_ntuples/V21_L1/V21a/
# outDir=/gpfs/atlas/public/ttRes_ntuples/V21_L1/V21d/
# outDir=/gpfs/atlas/public/ttRes_ntuples/V21_L1/V21e/
outDir=/gpfs/atlas/public/ttRes_ntuples/V21_L1/V21a-syst/
# outDir=/gpfs/atlas/public/ttRes_ntuples/V21_L1/V21d-syst/
# outDir=/gpfs/atlas/public/ttRes_ntuples/V21_L1/V21e-syst/

# inDir=../grid/V21a/
# inDir=../grid/V21d/
# inDir=../grid/V21e/
inDir=../grid/V21a-syst/
# inDir=../grid/V21d-syst/
# inDir=../grid/V21e-syst/

mkdir ${outDir}
mkdir inputs
mkdir logs

samples=""
# samples+=" ttbar_dil_mca singleTop_mca"
# samples+=" data15"
# samples+=" data16"
# samples+=" data17"
# samples+=" data18"
samples+=" ttbar_dil_mca"
# samples+=" ttbar_dil_mcd"
# samples+=" ttbar_dil_mce"
# samples+=" zprime2000_mcd"
# samples+=" zprime3000_mcd"

for sample in ${samples}   # -- use this (by defining a list of samples above) if want to run this script only for a given list of samples
# for sample in `ls ${inDir}/`
do
    sample=${sample/".txt"/""}
    echo "+------------------------------+"
    echo "| Reading sample "${sample}
    echo "+------------------------------+"
    rm logs/${sample}.log
    rm ${outDir}/${sample}.*.root
    for dataset in `cat ${inDir}/${sample}.txt`
    do
        echo "  Linking dataset "${dataset}
        python links_rucio.py  ${dataset}  ${sample}  ${outDir} >> logs/${sample}.log
    done
    echo "Dumping linked file names into txt file..."
    rm inputs/${sample}.txt
    for file in `ls ${outDir}/ | grep ${sample}.`
    do
        echo ${outDir}/${file} >> inputs/${sample}.txt
    done
    echo "Done."
done
