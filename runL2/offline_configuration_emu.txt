##################################################
#    Configuration file for ttH-offline          #
##################################################
# Notes:                                         #
#             				         #
# Hash demarks the start of a comment            #
# All settings are "CONFIG param"                #
# Settings can be altered with command line args #
#  -o CONFIG param                               #
# All settings need to be predefined in the code #
##################################################

#NEvents 100

###Where to save everything
OutputDirectory Output
SampleName sample

###Sample options
SampleType MC
UseLargeJets false

###Which systematics to save, provide a full list
Systematics nominal
###Or provide a file
SystematicsFromFile false
SystematicsFile None

###Calculate a normalisation weight from
###totalEventsWeighted,Cutflow or none
Normalisation totalEventsWeighted

###Provide a file to configure the weights to save
WeightsFile weights.txt

###Can choose to provide a list of variables to decorate onto the event
VariableListToDecorate VariablesToRead.txt

###Save all variables decorated onto the event
SaveAllDecorations false
# SaveAllDecorations true
###Otherwise save variables listed in file
VariablesToSaveFile VariablesToSave.txt

###Save obejct four-momenta and met
SaveDefaultVariables true
# SaveDefaultVariables false

###B-tagging working points
BTaggingWP DL1r_77

###Define the regions of the form Name:"cut expression",Name2:"cut expression"
Regions emu_ttbar_dilep:"emu"
# Regions emu_ttbar_dilep:"(emu_2015 || emu_2016 || emu_2017 || emu_2018)"

###New options (recommended for TQPOffline)
#CustomObjects smtmu,klfitter,truthsmtmu
# UseTTHbbVariables false
CreateTempOutputFile false
CreateOutputFileFirst true
SaveAllObjectDecorations false

##################################################
# TOOL configuration				 #
#      						 #
# Notes:					 #
# 						 #
# Tools in ToolManager have a given setting key  #
# Set key to true to run the tool                #
# Additional options may also be set here        #
##################################################

###Dictionaries
Dictionaries TQPTtbarResDilep

###Custom settings definition
CustomSettings ToolDilep.TurnOn,NeutrinoWeighting.TurnOn,NeutrinoWeighting.Setting,NeutrinoWeighting.MaxNjets

###Custom settings values
ToolDilep.TurnOn true
# NeutrinoWeighting.TurnOn true
# NeutrinoWeighting.Setting 1
# NeutrinoWeighting.MaxNjets 3

