files=""
files+=" offline_configuration*.txt"
files+=" VariablesToRead.txt VariablesToSave.txt weights.txt"
files+=" online_dilepton.txt"

here=`pwd`

for file in ${files}
do
    scp ${file} ../source/TQPTtbarResDilep/share/
done

cd ../source/TQPTtbarResDilep/
for file in ${files}
do
    git add share/${file}
done
git commit -m 'Configuration files updated'
git push
cd ${here}
